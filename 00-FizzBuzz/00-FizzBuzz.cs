﻿

namespace Katas.Week_1._01_FizzBuzz
{
    public class FizzBuzz
    {
        public static string[] Go(int n)
        {
            var result = new string[n];
            string tmpStr;
            for(var i=1; i <= n; i++)
            {
                if(i % 15 == 0)
                {
                    tmpStr = "FizzBuzz";
                }
                else if(i % 3 == 0)
                {
                    tmpStr = "Fizz";
                }
                else if(i % 5 == 0)
                {
                    tmpStr = "Buzz";
                }
                else
                {
                    tmpStr = i.ToString();
                }
                result[i-1] = tmpStr;
            }
            return result;
        }
    }
}
