﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");


namespace _04_BriefcaseLock.Solution
{
    public class Solution
    {
        public static int MinTurns(string startCombination, string desiredCombination)
        {
            if(startCombination.Length != desiredCombination.Length)
                throw new Exception("");
            int minTurns = 0;
            int current;
            int desiered;
            for(var i=0; i < startCombination.Length; i++)
            {
                current = startCombination[i] - '0';
                desiered = desiredCombination[i] - '0';

                minTurns += Math.Min
                (
                    forwardTurns(current, desiered),
                    backwardTurn(current, desiered)
                );
            }
            return minTurns;
        }

        private static int forwardTurns(int startPos, int goalPos)
        {
            if(startPos > goalPos)
            {
                return 10 - startPos + goalPos;
            }
            return goalPos - startPos;
        }

        private static int backwardTurn(int startPos, int goalPos)
        {
            if(startPos < goalPos)
            {
                return 10 - goalPos + startPos;
            }
            return startPos - goalPos;
        }
    }

}