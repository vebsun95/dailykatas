using Xunit;
using _04_BriefcaseLock.Solution;

namespace _04_BriefcaseLockTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("4089", "5672", 9)]
        [InlineData("1111", "1100", 2)]
        [InlineData("2391", "4984", 10)]
        [InlineData("0000", "1234", 10)]
        [InlineData("3333", "2300", 7)]
        [InlineData("9999", "9990", 1)]
        [InlineData("0000", "0009", 1)]
        [InlineData("8888", "8881", 3)]
        [InlineData("1111", "1118", 3)]
        public void Test1(string startComb, string desieredComb, int expected)
        {
            // Act
            var actual = Solution.MinTurns(startComb, desieredComb);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}