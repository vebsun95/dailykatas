using Xunit;

namespace _08_CelsiusToFahrenheitTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("35*C", "95*F")]
        [InlineData("19*F", "-7*C")]
        [InlineData("33", "Error")]
        [InlineData("-5*C", "23*F")]
        [InlineData("-10*F", "-23*C")]
        public void ConvertTests(string input, string expected)
        {
            var actual = Solution.Solution.Convert(input);

            Assert.Equal(expected, actual);
        }
    }
}