﻿// See https://aka.ms/new-console-template for more information
using System.Text.RegularExpressions;

Console.WriteLine(Solution.Solution.Convert("35*C"));

namespace Solution
{
    public class Solution
    {
        private static Regex regex = new Regex(@"(-?[0-9]+)\*([FC])");
        public static string Convert(string input)
        {
            if(!regex.IsMatch(input)) return "Error";
            var matches = regex.Match(input);
            var degress = int.Parse(matches.Groups[1].Value);
            var unitType = matches.Groups[2].Value;
            if(unitType == "F")
                return convertFahrenheitToCelsius(degress).ToString() + "*C";
            return convertCelsiusToFahrenheit(degress).ToString() + "*F";
        }
        private static int convertCelsiusToFahrenheit(int celsius)
        {
            return (int)Math.Round(celsius * 1.8 + 32);
        }
        private static int convertFahrenheitToCelsius(int fahrenheit)
        {
            return (int)Math.Round((fahrenheit - 32) / 1.8);
        }
    }
    
}