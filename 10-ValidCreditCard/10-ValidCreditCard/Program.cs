﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine(Solution.Solution.ValidateCard("1234567890123456"));

namespace Solution
{
    public class Solution
    {
        public static bool ValidateCard(string cardNumber)
        {
            if(cardNumber.Length < 14 || cardNumber.Length > 19)
                return false;
            int checkDigit = cardNumber[^1] - '0';
            char[] reversedCardNumber = cardNumber.Substring(0, cardNumber.Length - 1).ToArray();
            reversedCardNumber = reversedCardNumber.Reverse().ToArray();
            int[] numArray = new int[reversedCardNumber.Length];
            int digit;
            for(var i=0; i < reversedCardNumber.Length; i++)
            {
                digit = reversedCardNumber[i] - '0';
                if (i % 2 == 0)
                    digit = digit * 2;
                if (digit > 9)
                    digit = digit - 9;
                numArray[i] = digit;
            }
            var sum = numArray.Sum();
            return checkDigit == (10 - (sum % 10));
        }
    }
}