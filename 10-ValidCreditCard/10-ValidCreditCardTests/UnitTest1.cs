namespace _10_ValidCreditCardTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("79927398714", false)]
        [InlineData("79927398713", false)]
        [InlineData("709092739800713", true)]
        [InlineData("1234567890123456", false)]
        [InlineData("12345678901237", true)]
        [InlineData("5496683867445267", true)]
        [InlineData("4508793361140566", false)]
        [InlineData("376785877526048", true)]
        [InlineData("36717601781975", false)]
        public void Test1(string cardNumber, bool expected)
        {
            var actual = Solution.Solution.ValidateCard(cardNumber);

            Assert.Equal(expected, actual); 
        }
    }
}