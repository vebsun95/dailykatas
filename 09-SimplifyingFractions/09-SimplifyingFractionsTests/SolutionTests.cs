using Xunit;
using _09_SimplifyingFractions.Solution;
namespace _09_SimplifyingFractionsTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("4/6", "2/3")]
        [InlineData("10/11", "10/11")]
        [InlineData("100/400", "1/4")]
        [InlineData("16/15", "16/15")]
        [InlineData("16/12", "4/3")]
        [InlineData("8/4", "2")]
        [InlineData("0/5", "0")]
        [InlineData("5/0", "Error!")]
        [InlineData("42", "Error!")]
        [InlineData("-13/12", "-13/12")]
        [InlineData("-16/12", "-4/3")]
        [InlineData("-16/-12", "-4/-3")]

        public void Simplify(string fraction, string expected)
        {
            var actual = Solution.Simplify(fraction);

            Assert.Equal(expected, actual);
        }
    }
}