﻿// See https://aka.ms/new-console-template for more information
using System.Text.RegularExpressions;
using static System.Numerics.BigInteger;

Console.WriteLine("Hello, World!");

namespace _09_SimplifyingFractions.Solution
{
    public class Solution
    {
        private static Regex regex = new Regex(@"(-?[0-9]+)\/(-?[0-9]+)");
        public static string Simplify(string fraction)
        {
            if(!regex.IsMatch(fraction)) return "Error!";
            
            var match = regex.Match(fraction);
            var numerator = int.Parse(match.Groups[1].Value);
            var denominator = int.Parse(match.Groups[2].Value);
            if(denominator == 0) return "Error!";
            var gcd = (int)GreatestCommonDivisor(numerator, denominator);
            if(gcd == denominator)
                return (numerator / gcd).ToString();
            numerator /= gcd;
            denominator /= gcd;

            return $"{numerator}/{denominator}";
        }
    }
}