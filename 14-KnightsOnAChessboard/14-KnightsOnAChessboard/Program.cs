﻿// See https://aka.ms/new-console-template for more information
using System;
using _14_KnightsOnAChessboard.Board;

var a = new KnightsOnAChessboard();
System.Console.WriteLine( a.CannotCapture(new int[,] {
    { 0, 0, 0, 1, 0, 0, 0, 0 },
    { 0, 0, 0, 0, 0, 0, 0, 0 },
    { 0, 1, 0, 0, 0, 1, 0, 0 },
    { 0, 0, 0, 0, 1, 0, 1, 0 },
    { 0, 1, 0, 0, 0, 1, 0, 0 },
    { 0, 0, 0, 0, 0, 0, 0, 0 },
    { 0, 1, 0, 0, 0, 0, 0, 1 },
    { 0, 0, 0, 0, 1, 0, 0, 0 }
}));

System.Console.WriteLine( a.CannotCapture(new int[,] {
    {1, 0, 1, 0, 1, 0, 1, 0},
    {0, 1, 0, 1, 0, 1, 0, 1},
    {1, 0, 1, 0, 1, 0, 1, 0},
    {0, 0, 0, 1, 0, 1, 0, 1},
    {1, 0, 0, 0, 1, 0, 1, 0},
    {0, 0, 0, 0, 0, 1, 0, 1},
    {1, 0, 1, 0, 1, 0, 1, 0},
    {1, 0, 0, 1, 0, 0, 0, 1}
}));

namespace _14_KnightsOnAChessboard.Board
{
    public class KnightsOnAChessboard
    {
        private static List<Func<Position, Position>> possibleHorseMoves = new()
        {
            horseMoveUpAndRight,
            horseMoveUpAndLeft,
            horseMoveRightAndUp,
            horseMoveRightAndDown,
            horseMoveDownAndRight,
            horseMoveDownAndLeft,
            horseMoveLeftAndUp,
            horseMoveLeftAndDown,
        };
        public bool CannotCapture(int[,] board)
        {
            if(board.Length > 64 || board.GetUpperBound(0) > 8 || board.GetUpperBound(1) > 8)
                throw new ArgumentException("Invalid board");
            
            Position currentPosition;
            Position newPosition;
            for(int y=0; y < 8; y++)
            {
                for(int x=0; x < 8; x++)
                {
                    if(board[y, x] == 0) continue;
                    foreach(var possibleHorseMove in possibleHorseMoves)
                    {
                        currentPosition = new Position() {X=x, Y=y};
                        newPosition = possibleHorseMove.Invoke(currentPosition);
                        if(!ValidatePosition(ref board, newPosition))
                            continue;
                        
                        if(board[newPosition.Y, newPosition.X] == 1)
                        {
                            Console.WriteLine($"Horse at position: {currentPosition} can attack horse at position: {newPosition}");
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private static bool ValidatePosition(ref int[,] board, Position position)
        {
            if(position.X < 0 || position.Y < 0)
                return false;
            if(position.X >= 8 || position.Y >= 8) // The board is 8x8;
                return false;
            return true;
        }
        private static Position horseMoveUpAndRight(Position position) => new Position() { X = position.X + 1, Y = position.Y - 2 };
        private static Position horseMoveUpAndLeft(Position position) => new Position() { X = position.X - 1, Y = position.Y - 2 };
        private static Position horseMoveRightAndUp(Position position) => new Position() { X = position.X + 2, Y = position.Y - 1 };
        private static Position horseMoveRightAndDown(Position position) => new Position() { X = position.X + 2, Y = position.Y + 1 };
        private static Position horseMoveDownAndRight(Position position) => new Position() { X = position.X + 1, Y = position.Y + 2 };
        private static Position horseMoveDownAndLeft(Position position) => new Position() { X = position.X - 1, Y = position.Y + 2 };
        private static Position horseMoveLeftAndUp(Position position) => new Position() { X = position.X - 2, Y = position.Y - 1 };
        private static Position horseMoveLeftAndDown(Position position) => new Position() { X = position.X - 2, Y = position.Y + 1 };
        internal struct Position
        {
            public int X { get; set; }
            public int Y { get; set; }
            public override string ToString()
            {
                return $"({X}, {Y})";
            }
        }
    }

}