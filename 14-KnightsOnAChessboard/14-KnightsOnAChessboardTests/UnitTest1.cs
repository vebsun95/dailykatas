using Xunit;
using _14_KnightsOnAChessboard.Board;

namespace _14_KnightsOnAChessboardTests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var board = new int[,]
            {
                { 0, 0, 0, 1, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 1, 0, 0, 0, 1, 0, 0 },
                { 0, 0, 0, 0, 1, 0, 1, 0 },
                { 0, 1, 0, 0, 0, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 1, 0, 0, 0, 0, 0, 1 },
                { 0, 0, 0, 0, 1, 0, 0, 0 }
            };
            var checker = new KnightsOnAChessboard();
            var expected = true;

            var actual = checker.CannotCapture(board);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Test2()
        {
            var board = new int[,]
            {
                {1, 0, 1, 0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1, 0, 1, 0},
                {0, 0, 0, 1, 0, 1, 0, 1},
                {1, 0, 0, 0, 1, 0, 1, 0},
                {0, 0, 0, 0, 0, 1, 0, 1},
                {1, 0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 0, 1}
            };
            var checker = new KnightsOnAChessboard();
            var expected = false;

            var actual = checker.CannotCapture(board);

            Assert.Equal(expected, actual);
        }
    }
}