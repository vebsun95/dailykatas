

namespace _06_EnglishToPigLatinTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("flag", "agflay")]
        [InlineData("Apple", "Appleyay")]
        [InlineData("button", "uttonbay")]
        [InlineData("", "")]
        public void TranslateWordTests(string word, string expected)
        {
            var actual = Solution.Solution.TranslateWord(word);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("I like to eat honey waffles.", "Iyay ikelay otay eatyay oneyhay afflesway.")]
        [InlineData("Do you think it is going to rain today?", "Oday ouyay inkthay ityay isyay oinggay otay ainray odaytay?")]

        public void TranslateSentenceTests(string sentence, string expected)
        {
            var actual = Solution.Solution.TranslateSentence(sentence);

            Assert.Equal(expected, actual);
        }
    }
}