﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

namespace Solution
{
    public class Solution
    {
        private static HashSet<char> vowels = new HashSet<char>() {'a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U'};
        public static string TranslateWord(string word)
        {
            if(string.IsNullOrEmpty(word)) return string.Empty;

            string punctuation = string.Empty;
            if(wordEndsWithPunctuation(ref word))
            {
                punctuation = takeOutPunctuationFromWord(ref word);
            }
            if(wordStartsWithVowel(word))
                return translateWordThatStartsWithVowel(word) + punctuation;

            return translateWordThatStartsWithConsonants(word) + punctuation;
        }

        public static string TranslateSentence(string sentence)
        {
            return String.Join(" ", sentence.Split(" ").Select(word => TranslateWord(word)));
        }

        private static bool wordStartsWithVowel(string word)
        {
            return vowels.Contains(word[0]);
        }
        private static string translateWordThatStartsWithVowel(string word)
        {
            return word + "yay";
        } 
        private static string translateWordThatStartsWithConsonants(string word)
        {
            int numberOfConstants = findNumberOfConsonantsInStartOfWord(word);
            string startingConstants = word.Substring(0, numberOfConstants);
            string remainingWord = word.Substring(numberOfConstants);
            if(isCapitalized(word))
                swapCapitalizedLetter(ref startingConstants, ref remainingWord);
            return remainingWord + startingConstants + "ay";
        }

        private static bool isCapitalized(string word)
        {
            return word[0] < 97; // 97 is a;
        }

        private static void swapCapitalizedLetter(ref string startingConstants, ref string remainingWord)
        {
            startingConstants = char.ToLower(startingConstants[0]) + startingConstants.Substring(1);
            remainingWord = char.ToUpper(remainingWord[0]) + remainingWord.Substring(1);
        }
        private static int findNumberOfConsonantsInStartOfWord(string word)
        {
            int res = 0;
            while(!vowels.Contains(word[res]))
            {
                res++;
            }
            return res;
        }
        private static bool wordEndsWithPunctuation(ref string word)
        {
            return !char.IsLetter(word.Last());
        }

        private static string takeOutPunctuationFromWord(ref string word)
        {
            var punctuation = word.Last().ToString();
            word = word.Substring(0, word.Length - 1);
            return punctuation;
        }
    }
}