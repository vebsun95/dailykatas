﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");
//Console.WriteLine(Solution.Solution.PalendromeTimeStamps(2,12,22,4,35,10));
Console.WriteLine(Solution.Solution.PalendromeTimeStamps(12,12,12,13,13,13));
//Console.WriteLine(Solution.Solution.PalendromeTimeStamps(6,33,15,9,55,10));

namespace Solution
{
    public class Solution
    {
        // 

        // 12:22:21
        // 12:33:21
        // 12:44:21
        // 12:55:21
        // 13:00:31
        // 13:11:31

        // 13 + 31 = 42;
        // 12 + 21 = 33;
        private static HashSet<int> invalidPalindromehours = new HashSet<int> {6, 7,8,9,16,17,18,19};

        public static int PalendromeTimeStamps(int hourStart, int minStart, int secStart, int hourEnd, int minEnd, int secEnd)
        {
            int hour;
            int nrOfPalendromeTimestamps = 0;
            for(hour=hourStart; hour != (hourEnd + 1); hour = (hour + 1) % 24)
            {
                if(invalidPalindromehours.Contains(hour)) continue;
                var hourReversed = reverseInt(hour);
                // In an intermedia hour
                if(hour != hourStart && hour != hourEnd)
                {
                    nrOfPalendromeTimestamps += 6;
                }
                // At the first hour which is != from the last hour
                else if(hour == hourStart && hour != hourEnd)
                {
                    nrOfPalendromeTimestamps += ((60 - secStart) / 11) + 1;
                    if(secStart > hourReversed)
                        nrOfPalendromeTimestamps--;
                }
                // At the last hour which is != from the first hour
                else if(hour != hourStart && hour == hourEnd)
                {
                    nrOfPalendromeTimestamps += secEnd / 11;
                    if(secStart > hourReversed)
                        nrOfPalendromeTimestamps--;

                }
                // At the first and the last hour at once
                else if(hour == hourStart && hour == hourEnd)
                {

                }
            }
            return nrOfPalendromeTimestamps;
        }

        private static int reverseInt(int original)
        {
            int reversed = 0;
            while(original > 0)
            {
                reversed = reversed * 10 + original % 10;
                original /= 10;
            }
            return reversed;
        }
    } 
}