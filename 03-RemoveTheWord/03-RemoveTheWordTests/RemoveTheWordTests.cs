using RemoveTheWord.Solution;
using Xunit;

namespace _03_RemoveTheWordTests
{
    public class RemoveTheWordTests
    {
        [Theory]
        [InlineData(new char[] { 's', 't', 'r', 'i', 'n', 'g', 'w' }, "string", new char[] { 'w' })]
        [InlineData(new char[] { 'b', 'b', 'l', 'l', 'g', 'n', 'o', 'a', 'w' }, "balloon", new char[] { 'b', 'g', 'w' })]
        [InlineData(new char[] { 'a', 'n', 'r', 'y', 'o', 'w' }, "norway", new char[] { })]
        [InlineData(new char[] { 't', 't', 'e', 's', 't', 'u' }, "testing", new char[] { 't', 'u' })]
        [InlineData(new char[] { 'a', 'a', 'a', 'a' }, "aa", new char[] { 'a', 'a' })]
        public void Tests(char[] input1, string input2, char[] expected)
        {
            // Act
            var actual = Solution.RemoveLetters(input1, input2);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}