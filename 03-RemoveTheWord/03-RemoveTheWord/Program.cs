﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");


namespace RemoveTheWord.Solution
{
    public class Solution
    {
        public static char[] RemoveLetters(char[] chars, string charsToRemove)
        {
            List<char> returnArray = new();
            var multiSet = new Dictionary<char, int>();
            foreach (char c in chars)
            {
                if (multiSet.ContainsKey(c))
                    multiSet[c]++;
                else
                    multiSet[c] = 1;
            }
            foreach(char c in charsToRemove)
            {
                if (multiSet.ContainsKey(c))
                    multiSet[c]--;
            }
            foreach(var(c, count) in multiSet)
            {
                for(var i=count; i > 0; i--)
                {
                    returnArray.Add(c);
                }
            }
            return returnArray.ToArray();
        }
    } 
}