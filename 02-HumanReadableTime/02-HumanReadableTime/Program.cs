﻿using _02_HumanReadableTime.Solution;

Console.WriteLine(Solution.ConvertSecondsToHumanReadableFormat(6969));



namespace _02_HumanReadableTime.Solution
{
    public class Solution
    {
        private static readonly int SECOUNDS_IN_ONE_MINUTE = 60;
        private static readonly int SECOUNDS_IN_ONE_HOUR   = 60 * 60;

        /// <summary>
        /// Converts an interger representing secounds into human readable format HH:MM:SS
        /// </summary>
        /// <param name="nSeconds"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static string ConvertSecondsToHumanReadableFormat(int nSeconds)
        {
            if (nSeconds < 0 || nSeconds > 359999) throw new ArgumentException();

            var hours = nSeconds / SECOUNDS_IN_ONE_HOUR;
            var minutes = (nSeconds - (SECOUNDS_IN_ONE_HOUR * hours)) / SECOUNDS_IN_ONE_MINUTE;
            var seconds = nSeconds - (SECOUNDS_IN_ONE_HOUR * hours) - (SECOUNDS_IN_ONE_MINUTE * minutes);

            return ($"{hours:00}:{minutes:00}:{seconds:00}");
        }
    }
}

