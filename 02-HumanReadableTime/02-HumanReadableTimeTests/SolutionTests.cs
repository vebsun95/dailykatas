using Xunit;
using _02_HumanReadableTime.Solution;

namespace _02_HumanReadableTimeTests
{
    public class SolutionTests
    {
        [Theory]
        [InlineData(0, "00:00:00")]
        [InlineData(5, "00:00:05")]
        [InlineData(60, "00:01:00")]
        [InlineData(86399, "23:59:59")]
        [InlineData(359999, "99:59:59")]
        [InlineData(62, "00:01:02")]
        [InlineData(3600, "01:00:00")]
        [InlineData(7200, "02:00:00")]
        public void ConvertSecondsToHumanReadableFormatTests(int nSecounds, string expected)
        {
            // act
            var actual = Solution.ConvertSecondsToHumanReadableFormat(nSecounds);

            // Assert
            Assert.Equal(expected, actual);

        }
    }
}