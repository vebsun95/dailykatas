﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

namespace _05_FruitSmoothie.Smoothie
{
    public class Smoothie
    {
        private static Dictionary<string, double> IngredientsCost = new() 
        {
            { "Strawberries" , 1.50}, 
            { "Banana" , 0.50}, 
            { "Mango" , 2.50}, 
            { "Blueberries" , 1.00}, 
            { "Raspberries" , 1.00}, 
            { "Apple" , 1.75}, 
            { "Pineapple" , 3.50}, 
        };

        private static Dictionary<string, bool> isVeganOptions = new()
        {
            { "soy", true },
            { "oat", true },
            { "cow", false },
        };
        private bool isVegan = false;

        public Smoothie(string[] ingredients)
        {
            foreach(var ingredient in ingredients)
            {
                if (isVeganOptions.ContainsKey(ingredient))
                {
                    isVegan = isVeganOptions[ingredient];
                }
            }
            Ingredients = ingredients.Where(i => IngredientsCost.ContainsKey(i)).ToArray();
        }
        public string[] Ingredients {get; init;}
        private double cost
        {
            get
            {
                double totalCost = 0;
                foreach(var ingredient in Ingredients)
                {
                    if(IngredientsCost.ContainsKey(ingredient))
                        totalCost += IngredientsCost[ingredient];
                }
                return totalCost;
            }
        }
        public string GetCost
        {
            get 
            {
                return $"£{cost:0.00}";
            }
        }

        public string GetPrice
        {
            get
            {
                var price = cost + cost * 1.5;
                return $"£{price:0.00}";
            }
        }

        public string GetName
        {
            get
            {
                var smoothieName = "";
                string ingredientStr = "";
                Array.Sort(Ingredients);
                foreach(var ingredient in Ingredients)
                {
                    if(ingredient.Contains("berries"))
                    {
                        smoothieName += ingredient.Substring(0, ingredient.IndexOf("berries")) + "berry ";
                    }
                    else
                    {
                        smoothieName += ingredient + " ";
                    }
                }
                if(Ingredients.Length > 1)
                    return smoothieName + "Fusion";
                return smoothieName + "Smoothie";
            }
        }

    }

    public enum INGREDIENTS
    {
        Strawberries,
        Banana,
        Mango,
        Bluberries,
        Raspberries,
        Apple,
        Pineapple,
    }
}