using Xunit;
using _05_FruitSmoothie.Smoothie;

namespace _05_FruitSmoothieTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(new string[] {"Banana"}, "£0.50" )]
        [InlineData(new string[] { "Raspberries", "Strawberries", "Blueberries", "Cow" }, "£3.50")]
        [InlineData(new string[] { "Blueberries", "Banana", "Oat" }, "£1.50")]
        public void GetCost(string[] ingredients, string expected)
        {
            var s1 = new Smoothie(ingredients);

            var actual = s1.GetCost;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(new string[] { "Banana" }, "£1.25")]
        [InlineData(new string[] { "Raspberries", "Strawberries", "Blueberries", "Cow" }, "£8.75")]
        [InlineData(new string[] { "Blueberries", "Banana", "Oat" }, "£3.75")]
        public void GetPrice(string[] ingredients, string expected)
        {
            var s1 = new Smoothie(ingredients);

            var actual = s1.GetPrice;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(new string[] { "Banana" }, "Banana Smoothie")]
        [InlineData(new string[] { "Raspberries", "Strawberries", "Blueberries", "Cow" }, "Blueberry Raspberry Strawberry Fusion")]
        [InlineData(new string[] { "Blueberries", "Banana", "Oat" }, "Banana Blueberry Fusion")]
        public void GetName(string[] ingredients, string expected)
        {
            var s1 = new Smoothie(ingredients);

            var actual = s1.GetName;

            Assert.Equal(expected, actual);
        }
    }
}