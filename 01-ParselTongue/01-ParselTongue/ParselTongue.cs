﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ParselTongue
{
    public class ParselTongue
    {
        public static bool IsParselTongue(string sentence)
        {

            string[] words = sentence.ToLower().Split(" ");
            for(int i = 0; i < words.Length; i++)
            {
                if (words[i].Contains("s"))
                {
                    if (!words[i].Contains("ss")) return false;
                }
            }
            return true;
        }
    }
}
