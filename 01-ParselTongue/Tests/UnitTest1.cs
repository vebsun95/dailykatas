using System;
using Xunit;
using _01_ParselTongue;

namespace Tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("Sshe ssselects to eat that apple.", true)]
        [InlineData("She sselects to eat that apple", false)]
        [InlineData("You ssseldom sssspeak sso boldly, ssso messmerizingly.", true)]
        [InlineData("Steve likes to eat pancakes", false)]
        [InlineData("Sssteve likess to eat pancakess", true)]
        [InlineData("Beatrice samples lemonade.", false)]
        [InlineData("Beatrice enjoysss lemonade", true)]
        public void Test1(string input, bool result)
        {
            Assert.Equal(ParselTongue.IsParselTongue(input), result);
        }
    }
}
