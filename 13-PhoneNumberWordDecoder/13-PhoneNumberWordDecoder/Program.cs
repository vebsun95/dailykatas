﻿// See https://aka.ms/new-console-template for more information
using System.Text.RegularExpressions;

Console.WriteLine(_13_PhoneNumberWordDecoder.Soltuion.PhoneNumberWordDecoder.TextToNum("123-647-EYES"));
Console.WriteLine(_13_PhoneNumberWordDecoder.Soltuion.PhoneNumberWordDecoder.DecodeMessage("777733663444664"));

namespace _13_PhoneNumberWordDecoder.Soltuion
{
    public class PhoneNumberWordDecoder
    {
        private static Regex text2numRegex = new Regex(@"[(A-Z0-9-|\(A-Z0-9\))]+([A-Z0-9]+)-([A-Z0-9]+)");
        private static Dictionary<char, char> num2charDecoder = new Dictionary<char, char>()
        {
            { 'A', '2' },
            { 'B', '2' },
            { 'C', '2' },
            { 'D', '3' },
            { 'E', '3' },
            { 'F', '3' },
            { 'G', '4' },
            { 'H', '4' },
            { 'I', '4' },
            { 'J', '5' },
            { 'K', '5' },
            { 'L', '5' },
            { 'M', '6' },
            { 'N', '6' },
            { 'O', '6' },
            { 'P', '7' },
            { 'Q', '7' },
            { 'R', '7' },
            { 'S', '7' },
            { 'T', '8' },
            { 'U', '8' },
            { 'V', '8' },
            { 'W', '9' },
            { 'X', '9' },
            { 'Y', '9' },
            { 'Z', '9' },
        };
        public static string TextToNum(string text)
        {
            if(!text2numRegex.IsMatch(text))
                return "Invalid text!";

            foreach(var cappitalLetter in num2charDecoder.Keys)
                if(text.Contains(cappitalLetter))
                {
                    text = text.Replace(cappitalLetter, num2charDecoder[cappitalLetter]);
                }

            return text;
        }
        private static Dictionary<string, string> DecodeMessageDecoder = new ()
        {
            {"00", " "},
            {"1111", "?"},
            {"7777", "S"},
            {"9999", "Z"},
            {"111", "!"},
            {"222", "C"},
            {"333", "F"},
            {"444", "I"},
            {"555", "L"},
            {"666", "O"},
            {"777", "R"},
            {"888", "V"},
            {"999", "Y"},
            {"11", ","},
            {"22", "B"},
            {"33", "E"},
            {"44", "H"},
            {"55", "K"},
            {"66", "N"},
            {"77", "Q"},
            {"88", "U"},
            {"99", "X"},
            {"1", "."},
            {"2", "A"},
            {"3", "D"},
            {"4", "G"},
            {"5", "J"},
            {"6", "M"},
            {"7", "P"},
            {"8", "T"},
            {"9", "W"},
            {"0", ""},
        };
        public static string DecodeMessage(string number)
        {
            if (!Regex.IsMatch(number, "[0-9]+"))
                return "Invalid";
            foreach(string numberCombo in DecodeMessageDecoder.Keys)
            {
                if(number.Contains(numberCombo))
                    number = number.Replace(numberCombo, DecodeMessageDecoder[numberCombo]);
            }
            return number;
        }
    }
}