using Xunit;
using _13_PhoneNumberWordDecoder.Soltuion;

namespace _13_PhoneNumerWordDecoderTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("123-647-EYES", "123-647-3937")]
        [InlineData("(325)444-TEST", "(325)444-8378")]
        [InlineData("653-TRY-THIS", "653-879-8447")]
        [InlineData("435-224-7613", "435-224-7613")]
        public void Test1(string text, string expected)
        {
            var actual = PhoneNumberWordDecoder.TextToNum(text);

            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData("526", "JAM")]
        [InlineData("777733663444664", "SENDING")]
        [InlineData("2202022999", "BABY")]
        [InlineData("443355505556660096667775553", "HELLO WORLD")]
        [InlineData("44335550555666111", "HELLO!")]
        [InlineData("44335550555666110056660661", "HELLO, JON.")]
        [InlineData("663390074466606633110094466600344477771111", "NEW PHONE, WHO DIS?")]
        public void Test2(string text, string expected)
        {
            var actual = PhoneNumberWordDecoder.DecodeMessage(text);

            Assert.Equal(expected, actual);
        }
    }
}