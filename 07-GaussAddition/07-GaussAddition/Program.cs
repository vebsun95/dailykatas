﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

namespace _07_GaussAddition.Sum
{
    public class Sum
    {
        public static int Gauss(int start, int end = 0)
        {
            if(end < start)
                swap(ref start, ref end);
            return ParitalSum(end) - ParitalSum(start - 1);
        }
        private static void swap(ref int start, ref int end)
        {
            int tmp = start;
            start = end;
            end = tmp;
        }
        private static int ParitalSum(int n)
        {
            return (n * (n + 1) ) / 2;
        }
    } 
}