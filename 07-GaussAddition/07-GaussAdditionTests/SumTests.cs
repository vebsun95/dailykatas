namespace _07_GaussAdditionTests;
using _07_GaussAddition.Sum;
using Xunit;

public class SumTests
{
    [Theory]
    [InlineData(100, 5050)]
    [InlineData(0, 0)]
    public void Gauss_OneInput(int input, int expected)
    {
        var actual = Sum.Gauss(input);
        Assert.Equal(expected, actual);
    }


    [Theory]
    [InlineData(5001, 7000, 12001000)]
    [InlineData(1975, 165, 1937770)]
    [InlineData(5, 5, 5)]
    [InlineData(1, 0, 1)]
    [InlineData(-5, 10, 40)]
    [InlineData(-10, 5, -40)]
    public void Gauss_TwoInputs(int input1, int input2, int expected)
    {
        var actual = Sum.Gauss(input1, input2);
        Assert.Equal(expected, actual);
    }
}