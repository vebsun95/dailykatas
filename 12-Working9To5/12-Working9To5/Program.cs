﻿// See https://aka.ms/new-console-template for more information
System.Console.WriteLine(Overtime.Overtime.OverTime(13.25, 15.0, 30.0, 1.5));
namespace Overtime
{
   public class Overtime
   {
    public static string OverTime(double start, double end, double hourlySalary, double overtimeMultiplier)
    {
        double pay = 0;
        double hourlyRate;
        double partialHourWorked;
        // If the first hour is partial, calculate the salary and increment starting hour
        if(start % 1 != 0)
        {
            partialHourWorked = 1 - (start - (int)start);
            pay += partialHourWorked * hourlySalary;
            if(start >= 17)
                pay *= overtimeMultiplier;
            start += partialHourWorked;
        }

        // Calculate the salary for all full intermediate hours;
        for(; start < (int)end; start++)
        {
            hourlyRate = hourlySalary;
            if(start >= 17)
                hourlyRate *= overtimeMultiplier;
            pay += hourlyRate;
        }

        // Calculate the final hour, accounting for partial hour;
        hourlyRate = hourlySalary;
        if(start >= 17)
            hourlyRate *= overtimeMultiplier;
        pay += (end - (int) end) * hourlyRate;


        return $"${pay:.00}";
    }

   } 
}