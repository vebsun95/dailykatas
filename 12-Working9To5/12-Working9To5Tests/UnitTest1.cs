using Xunit;

namespace _12_Working9To5Tests;
public class UnitTest1
{
    [Theory]
    [InlineData(9.0, 17.0, 30.0, 1.5, "$240.00")]
    [InlineData(9.5, 17.5, 30.0, 1.5, "$247.50")]
    [InlineData(16.0, 18.0, 30.0, 1.8, "$84.00")]
    [InlineData(13.25, 15.0, 30.0, 1.5, "$52.50")]
    public void Test1(double start, double end, double hourlyRate, double overtimeMultiplier, string expected)
    {
        var actual = Overtime.Overtime.OverTime(start, end, hourlyRate, overtimeMultiplier);

        Assert.Equal(expected, actual);
    }
}